using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public abstract class Animal : Actor
    {
        [Header("Vitals")] [SerializeField] private float hungerThreshold = 0.8f;
        [SerializeField] private float thirstThreshold = 0.8f;
        [SerializeField] private float reproductiveUrgeThreshold = 0.8f;

        private PathFinder pathFinder;
        private VitalStats vitals;
        private Mover mover;
        private Feeder feeder;
        private OffspringCreator offspringCreator;
        private Sensor sensor;
        protected List<Node> nodes = null;
        protected int moveCount;
        private IDrinkable drinkTarget;
        public Water DrinkTarget { get; set; }
        public string GizmoState;
        public Animal reproductionTarget;
        

        protected Statistics statistics;
        protected StateMachine stateMachine;
        protected PathFinder PathFinder => pathFinder;
        public VitalStats Vitals => vitals;
        protected Mover Mover => mover;
        protected Feeder Feeder => feeder;
        protected OffspringCreator OffspringCreator => offspringCreator;
        protected Sensor Sensor => sensor;
        private ISensor<Water> Water;
        private ISensor<Fox> sensedFoxes;
        private ISensor<Animal> sensedAnimals;
        public IReadOnlyList<GameObject> SensedObjects => Sensor.SensedObjects;
        public IReadOnlyList<Fox> SensedFoxes => sensedFoxes.SensedObjects;
        public IReadOnlyList<Animal> SensedAnimals => sensedAnimals.SensedObjects;

        public IReadOnlyList<Water> SensedWater => Water.SensedObjects;
        public bool IsHungry => vitals.Hunger > hungerThreshold;
        public bool IsThirsty => vitals.Thirst > thirstThreshold;
        public bool IsHorny => vitals.ReproductiveUrge > reproductiveUrgeThreshold;
        public bool IsDead => vitals.IsDead;

        //Author : Yannick Cote
        protected void Awake()
        {
            pathFinder = Finder.PathFinder;
            statistics = Finder.Statistics;
            vitals = GetComponentInChildren<VitalStats>();
            mover = GetComponentInChildren<Mover>();
            feeder = GetComponentInChildren<Feeder>();
            offspringCreator = GetComponentInChildren<OffspringCreator>();
            sensor = GetComponentInChildren<Sensor>();
            sensedFoxes = sensor.For<Fox>();
            Water = sensor.For<Water>();
            sensedAnimals = sensor.For<Animal>();
            DrinkTarget = null;
            reproductionTarget = null;
        }
        //Author : Jeammy Cote
        private void Start()
        {
            OnCreation();
        }

        //Author : Yannick Cote
        private void Update()
        {
#if UNITY_EDITOR
            try
            {
#endif
                stateMachine.Update();
#if UNITY_EDITOR
            }
            catch (Exception ex)
            {
                Debug.LogError($"{name} errored : {ex.Message}\n{ex.StackTrace}.", gameObject);
                gameObject.SetActive(false);
            }
#endif
        }
        
        //Author : Jeammy Côté 
        public bool Feed(IEatable eatable)
        {
            return Feeder.Eat(eatable);
        }
        
        //Author : Jeammy Côté 
        public bool Drink(IDrinkable drinkable)
        {
            return Feeder.Drink(drinkable);
        }
        
        //Author : Jeammy Côté
        public abstract WalkingState ReturnWalkingState();

        //Author : Jeammy Côté based from RandomMove
        //Modify by Yannick Cote 9/10
        public void MoveTo(List<Node> nodeList)
        {
            if (nodes != null)
            {
                if (moveCount < nodes.Count)
                {
                    if (!Mover.IsMoving)
                    {
                        Mover.MoveTo(nodes[moveCount].Position3D);
                        moveCount++;
                    }
                }
                else
                {
                    nodes = null;
                }
            }
            else
            {
                moveCount = 0;
                nodes = nodeList;
            }
        }
        //Author : Jeammy Côté based from RandomWalk
        public List<Node> FindPath(Vector3 destination)
        {
            return PathFinder.FindPath(Position,destination,1f);
        }

        //Author:Yannick Cote
        public void RandomMove()
        {
            if (nodes != null)
            {
                if (moveCount < nodes.Count)
                {
                    if (!Mover.IsMoving)
                    {
                        Mover.MoveTo(nodes[moveCount].Position3D);
                        moveCount++;
                    }
                }
                else
                {
                    nodes = null;
                }
            }
            else
            {
                moveCount = 0;
                nodes = RandomWalk(Position, 10);
            }
        }
        //Author:Yannick Cote
        private List<Node> RandomWalk(Vector3 start, int steps)
        {
            return PathFinder.FindRandomWalk(start, steps);
        }

        private void OnEnable()
        {
            vitals.OnDeath += OnDeath;

            if (vitals.IsDead) OnDeath();

        }

        private void OnDisable()
        {
            vitals.OnDeath -= OnDeath;
        }

        protected virtual void OnDeath()
        {
            Destroy();
        }
        
        protected virtual void OnCreation()
        {
        }

        [ContextMenu("Destroy")]
        private void Destroy()
        {
            Destroy(gameObject);
        }

#if UNITY_EDITOR
        //Author : Yannick Cote
        private void OnDrawGizmosSelected()
        {
            GizmosExtensions.DrawText(Position, GizmoState);
            
            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (i+1 < nodes.Count)
                    {
                        GizmosExtensions.DrawArrow(nodes[i].Position3D,nodes[i+1].Position3D,Color.blue);
                    }
                }
            }
        }
#endif
    }
}