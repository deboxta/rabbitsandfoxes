using System.Collections.Generic;
using Game.States;
using Play.Sql.Tables;
using UnityEngine;
//Author : Yannick Cote, Jeammy Cote

namespace Game
{
    public sealed class Bunny : Animal, IPrey
    {
        [Header("Other")] [SerializeField] [Range(0f, 1f)] private float nutritiveValue = 1f;

        public bool IsEatable => !Vitals.IsDead;
        private ISensor<IVegetable> vegetableSensor;
        public IVegetable targetVegetable;
        public BunnyTable bunnyTable;

        public IReadOnlyList<IVegetable> SensedVegetable => vegetableSensor.SensedObjects;


        protected new void Awake()
        {
            base.Awake();
            
            bunnyTable = new BunnyTable();
            
            vegetableSensor = Sensor.For<IVegetable>();
            targetVegetable = null;
            
            stateMachine = new StateMachine(new BunnyWalkingState(this));
        }

        public void Flee()
        {
            if (SensedFoxes.Count > 0)
            {
                if (!Mover.IsMoving)
                {
                    var predator = SensedFoxes.FindClosest(Position);
                    if (!IsDead && predator != null && !predator.IsDead)
                    {
                        var findFleePath = PathFinder.FindFleePath(Position, predator.Position);
                        if (findFleePath != null)
                        {
                            Mover.MoveTo(findFleePath.Position3D);
                        }
                    }
                }
            }
        }
        
        //When bunny return from flee state he start a new nodes path.
        public void ResetNodes()
        {
            nodes = null;
        }
        
        public List<Node> FindVegetablePath()
        {
            if (vegetableSensor.SensedObjects != null)
            {
                return PathFinder.FindPath(Position,targetVegetable.Position,1f);
            }
            return null;
        }

        public override WalkingState ReturnWalkingState()
        {
            return new BunnyWalkingState(this);
        }
        
        public IEffect Eat()
        {
            UpdateRepo(TypeOfDeath.EATED);
            
            Vitals.Die();

            return new LoseHungerEffect(nutritiveValue);
        }

        protected override void OnCreation()
        {
            bunnyTable.SpawnTime = (int)Time.realtimeSinceStartup;
            bunnyTable = statistics.CreateBunnyOnRepo(bunnyTable);
        }

        public void UpdateRepo(string typeOfDeath)
        {
            BunnyTable oldBunny = bunnyTable;
            bunnyTable.DeathTime = (int)Time.realtimeSinceStartup;
            bunnyTable.DeathType = typeOfDeath;
            statistics.WriteOnBunnyRepo(bunnyTable, oldBunny);
        }
        
        protected override void OnDeath()
        {
            if (Vitals.Hunger >= Vitals.HungerDeathThreshold)
            {
                UpdateRepo(TypeOfDeath.HUNGER);
            }

            if (Vitals.Thirst >= Vitals.ThirstDeathThreshold)
            {
                UpdateRepo(TypeOfDeath.THIRST);
            }
            base.OnDeath();
        }
        public bool CreateOffspring(Bunny otherBunny)
        {
            return OffspringCreator.CreateOffspringWith(otherBunny);
        }
    }
}