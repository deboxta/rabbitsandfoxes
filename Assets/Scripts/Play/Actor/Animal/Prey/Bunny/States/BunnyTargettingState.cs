﻿//Author : Yannick Cote

namespace Game.States
{
    public class BunnyTargettingState : BaseState
    {
        private Bunny bunny;
        public BunnyTargettingState(Bunny bunny)
        {
            this.bunny = bunny;

            if (bunny.SensedVegetable.Count > 0)
            {
                foreach (var vegetable in bunny.SensedVegetable)
                {
                    if (bunny.SensedVegetable != null && vegetable.IsEatable)
                    {
                        bunny.targetVegetable = vegetable;
                    }
                }
            }
        }
        public override IState Update()
        {
            if (bunny.IsHungry)
            {
                if (bunny.targetVegetable != null && bunny.targetVegetable.IsEatable)
                {
                    if (bunny.Feed(bunny.targetVegetable))
                    {
                        bunny.targetVegetable = null;
                        return new BunnyWalkingState(bunny);
                    }
                    bunny.MoveTo(bunny.FindVegetablePath());
                }
                else
                {
                    return new BunnySearchFoodState(bunny);
                }
            }
            else
            {
                return new BunnyWalkingState(bunny);
            }

            return this;
        }
        //Author : Jeammy Côté
        public override void Enter()
        {
            bunny.GizmoState = StatesTag.BUNNYTARGETINGSTATE;
        }
    }
}