﻿//Author : Yannick Cote
namespace Game.States
{
    public class BunnyWalkingState : WalkingState
    {
        private Bunny bunny;
        public BunnyWalkingState(Bunny bunny) : base(bunny)
        {
            this.bunny = bunny;
        }

        public override IState Update()
        {
            if (bunny.SensedFoxes.Count > 0)
            {
                return new BunnyFleeState(bunny);
            }
            if (bunny.IsHungry)
            {
                return new BunnySearchFoodState(bunny);
            }
            if (bunny.IsHorny)
            {
                return new BunnyHornyState(bunny);
            }
            return base.Update();
        }
        //Author : Jeammy Côté
        public override void Enter()
        {
            bunny.GizmoState = StatesTag.BUNNYWALKINGSTATE;
        }
    }
}