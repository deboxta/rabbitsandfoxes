﻿//Author : Yannick Cote

namespace Game.States
{
    public class BunnySearchFoodState : BaseState
    {
        private Bunny bunny;

        public BunnySearchFoodState(Bunny bunny)
        {
            this.bunny = bunny;
        }
        
        //Author : Yannick Côté
        //Modified by Jeammy Côté
        public override IState Update()
        {
            if (bunny.IsHungry)
            {
                if (bunny.SensedVegetable.Count == 0 || (bunny.targetVegetable != null && !bunny.targetVegetable.IsEatable))
                {
                    bunny.RandomMove();
                }
                else if (bunny.SensedVegetable.Count > 0)
                {
                    return new BunnyTargettingState(bunny);
                }
            }
            else
            {
                return new BunnyWalkingState(bunny);
            }
            return this;
        }
        //Author : Jeammy Côté
        public override void Enter()
        {
            bunny.GizmoState = StatesTag.BUNNYSEARCHFOODSTATE;
        }
    }
}