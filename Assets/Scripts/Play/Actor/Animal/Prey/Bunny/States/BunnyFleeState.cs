﻿//Author : Yannick Cote
namespace Game.States
{
    public class BunnyFleeState : BaseState
    {
        private Bunny bunny;
        
        public BunnyFleeState(Bunny bunny)
        {
            this.bunny = bunny;
        }
        public override IState Update()
        {
            if (bunny.SensedFoxes.Count > 0)
            {
                if (bunny != null)
                {
                    bunny.Flee();
                }
            }
            else
            {
                bunny.ResetNodes();
                return new BunnyWalkingState(bunny);
            }

            return this;
        }
        
        //Author : Jeammy Côté
        public override void Enter()
        {
            bunny.GizmoState = StatesTag.BUNNYFLEESTATE;
        }
    }
}