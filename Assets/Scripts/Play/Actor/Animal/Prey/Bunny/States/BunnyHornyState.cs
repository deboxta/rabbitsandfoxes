﻿//Author : Jeammy Côté

namespace Game
{
    public class BunnyHornyState : BaseState
    {
        private Bunny bunny;

        public BunnyHornyState(Bunny bunny)
        {
            this.bunny = bunny;
        }
        
        public override IState Update()
        {
            if (bunny.IsHorny)
            {
                if (bunny.reproductionTarget == null)
                {
                    foreach (var target in bunny.SensedAnimals)
                    {
                        if (target.GetType() == bunny.GetType() && target.IsHorny)
                        {
                            bunny.reproductionTarget = target;
                        }
                    }
                }
                if (bunny.reproductionTarget != null)
                {
                    if (bunny.CreateOffspring(bunny.reproductionTarget as Bunny))
                    {
                        bunny.reproductionTarget = null;
                        return bunny.ReturnWalkingState();
                    }
                    bunny.MoveTo(bunny.FindPath(bunny.reproductionTarget.Position));
                }
                bunny.RandomMove();
            }
            else
            {
                return bunny.ReturnWalkingState();
            }
            return this;
        }
        
        public override void Enter()
        {
            bunny.GizmoState = StatesTag.BUNNYHORNYSTATE;
        }
    }
}
