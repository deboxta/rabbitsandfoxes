using System.Collections.Generic;
using Play.Sql.Tables;
using UnityEngine; 
//Modify by Yannick Cote 9/10
//Author : Jeammy Côté

namespace Game
{
    public sealed class Fox : Animal, IPredator
    {
        private ISensor<Bunny> bunnySensor;
        public IReadOnlyList<Bunny> SensedBunny => bunnySensor.SensedObjects;
        public Bunny Prey { get; set; }
        public FoxTable FoxTable { get; set; }
        
        protected new void Awake()
        {
            base.Awake();
            bunnySensor = Sensor.For<Bunny>();
            Prey = null;
            FoxTable = new FoxTable();
            stateMachine = new StateMachine(new FoxWalkingState(this));
        }
        public List<Node> FindPreyPath()
        {
            if (bunnySensor.SensedObjects != null && SensedBunny.Count > 0)
            {
                return PathFinder.FindPath(Position, SensedBunny[0].Position, 1f);
            }

            return null;
        }

        public override WalkingState ReturnWalkingState()
        {
            return new FoxWalkingState(this);
        }
        
        protected override void OnCreation()
        {
            FoxTable.SpawnTime = (int)Time.realtimeSinceStartup;
            FoxTable = statistics.CreateFoxOnRepo(FoxTable);
        }
        
        public void UpdateRepo(string typeOfDeath)
        {
            FoxTable oldFox = FoxTable;
            FoxTable.DeathTime = (int)Time.realtimeSinceStartup;
            FoxTable.DeathType = typeOfDeath;
            statistics.WriteOnFoxRepo(FoxTable, oldFox);
        }
        
        protected override void OnDeath()
        {
            if (Vitals.Hunger >= Vitals.HungerDeathThreshold)
            {
                UpdateRepo(TypeOfDeath.HUNGER);
            }

            if (Vitals.Thirst >= Vitals.ThirstDeathThreshold)
            {
                UpdateRepo(TypeOfDeath.THIRST);
            }
            base.OnDeath();
        }
        
        public bool CreateOffspring(Fox otherFox)
        {
            return OffspringCreator.CreateOffspringWith(otherFox);
        }
    }
}