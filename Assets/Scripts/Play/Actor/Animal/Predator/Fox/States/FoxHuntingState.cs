﻿//Author : Jeammy Côté


namespace Game
{
    public class FoxHuntingState : BaseState
    {
        private readonly Fox fox;

        public FoxHuntingState(Fox fox)
        {
            this.fox = fox;
        }

        public override IState Update()
        {
            if (fox.IsHungry)
            {
                if (fox.Prey == null && fox.SensedBunny.Count > 0)
                {
                    fox.Prey = fox.SensedBunny.FindClosest(fox.Position);
                }
                if (fox.Prey != null)
                {
                    if (fox.Feed(fox.Prey))
                    {
                        fox.Prey = null;
                        return fox.ReturnWalkingState();
                    }
                    fox.MoveTo(fox.FindPreyPath());
                }
                else
                {
                    return new FoxSearchFoodState(fox);
                }
            }
            else
            {
                return fox.ReturnWalkingState();
            }
            return this;
        }
        public override void Enter()
        {
            fox.GizmoState = StatesTag.FOXHUNTINGSTATE;
        }
    }
}