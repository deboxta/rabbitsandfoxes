﻿//Author : Jeammy Côté

namespace Game
{
    public class FoxSearchFoodState : BaseState
    {
        private readonly Fox fox;

        public FoxSearchFoodState(Fox fox)
        {
            this.fox = fox;
        }

        public override IState Update()
        {
            if (fox.IsHungry)
            {
                if (fox.SensedBunny.Count == 0)
                {
                    fox.RandomMove();
                }
                else if (fox.SensedBunny.Count > 0)
                {
                    return new FoxHuntingState(fox);
                }
            }
            else
            {
                return fox.ReturnWalkingState();
            }
            return this;
        }
        public override void Enter()
        {
            fox.GizmoState = StatesTag.FOXSEARCHFOODSTATE;
        }
    }
}