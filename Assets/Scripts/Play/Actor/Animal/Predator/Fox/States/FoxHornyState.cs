﻿//Author : Jeammy Côté

namespace Game
{
    public class FoxHornyState : BaseState
    {
        private Fox fox;

        public FoxHornyState(Fox fox)
        {
            this.fox = fox;
        }
        
        public override IState Update()
        {
            if (fox.IsHorny)
            {
                if (fox.reproductionTarget == null)
                {
                    foreach (var target in fox.SensedAnimals)
                    {
                        if (target.GetType() == fox.GetType() && target.IsHorny)
                        {
                            fox.reproductionTarget = target;
                        }
                    }
                }
                if (fox.reproductionTarget != null)
                {
                    if (fox.CreateOffspring(fox.reproductionTarget as Fox))
                    {
                        fox.reproductionTarget = null;
                        return fox.ReturnWalkingState();
                    }
                    fox.MoveTo(fox.FindPath(fox.reproductionTarget.Position));
                }
                fox.RandomMove();
            }
            else
            {
                return fox.ReturnWalkingState();
            }
            return this;
        }
        public override void Enter()
        {
            fox.GizmoState = StatesTag.FOXHORNYSTATE;
        }
    }
}