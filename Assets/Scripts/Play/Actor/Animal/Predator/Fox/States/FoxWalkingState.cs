﻿
namespace Game
{
    public class FoxWalkingState : WalkingState
    {
        private Fox fox;
        public FoxWalkingState(Fox fox) : base(fox)
        {
            this.fox = fox;
        }

        //Author : Yannick Cote
        public override IState Update()
        {
            if (fox.IsHungry)
            {
                return new FoxSearchFoodState(fox);
            }
            if (fox.IsHorny)
            {
                return new FoxHornyState(fox);
            }
            return base.Update();
        }
        //Author : Jeammy Côté
        public override void Enter()
        {
            fox.GizmoState = StatesTag.FOXWALKINGSTATE;
        }
    }
}