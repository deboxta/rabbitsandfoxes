﻿//Author : Yannick Cote
namespace Game
{
    public class StateMachine
    {
        private IState currentState;

        public StateMachine(IState startState)
        {
            currentState = startState;
            
            currentState.Enter();
        }

        public void Update()
        {
            var nextState = currentState.Update();

            if (nextState != currentState)
            {
                currentState?.Leave();
                currentState = nextState;
                currentState?.Enter();
            }
        }
    }
}