﻿using System.Linq;
//Author : Jeammy Côté

namespace Game
{
    public class SearchWaterState : BaseState
    {
        private readonly Animal animal;
        public SearchWaterState(Animal animal)
        {
            this.animal = animal;
            if (animal.SensedWater.Count > 0)
            {
                foreach (var water in animal.SensedWater)
                {
                    if (water != null)
                    {
                        animal.DrinkTarget = water.GetComponentInChildren<Water>();
                    }
                }
            }
        }

        public override IState Update()
        {
            if (animal.IsThirsty)
            {
                if (animal.SensedWater.Count > 0 && animal.DrinkTarget == null)
                {
                    animal.DrinkTarget = animal.SensedWater.First().GetComponentInChildren<Water>();
                }
                if (animal.SensedWater.Count > 0 && animal.DrinkTarget != null)
                {
                    if (animal.Drink(animal.DrinkTarget))
                    {
                        animal.DrinkTarget = null;
                        return animal.ReturnWalkingState();
                    }
                    animal.MoveTo(animal.FindPath(animal.DrinkTarget.Position));
                }
                else
                {
                    animal.RandomMove();
                }
            }
            else
            {
                return animal.ReturnWalkingState();
            }

            return this;
        }
        public override void Enter()
        {
            animal.GizmoState = StatesTag.SEARCHWATERSTATE;
        }
    }
}