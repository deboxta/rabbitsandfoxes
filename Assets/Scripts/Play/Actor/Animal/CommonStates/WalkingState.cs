﻿//Author : Yannick Cote

namespace Game
{
    public class WalkingState : BaseState
    {
        private Animal animal;

        public WalkingState(Animal animal)
        {
            this.animal = animal;
        }
        
        public override IState Update()
        {
            if (animal.IsThirsty)
            {
                return new SearchWaterState(animal);
            }
            animal.RandomMove();
            return this;
        }
    }
}