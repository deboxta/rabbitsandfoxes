﻿using System;
using System.Collections.Generic;
using System.Data.Common;
//Author : Yannick Cote
namespace Play.Sql
{
    public interface IRepository<T>
    {
        T Insert(T item);
        T Update(T item,  T itemToUpdate);
        T Select(T item);
        List<T> SelectAll();
        void Delete(T item);
    }
}