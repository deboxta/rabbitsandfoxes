﻿//Author : Yannick Cote
namespace Play.Sql
{
    public static class SqlCommands
    {
        //SIMULATION table commands
        public const string SIM_INSERT = "INSERT INTO simulation(seed) VALUES(?); SELECT last_insert_rowid()";
        public const string SIM_UPDATE = "UPDATE simulation SET seed = ? WHERE id = ?";
        public const string SIM_SELECT = "SELECT * FROM simulation WHERE id = ?";
        public const string SIM_SELECT_ALL = "SELECT * FROM simulation";
        public const string SIM_DELETE = "DELETE FROM simulation WHERE id = ?";

        //BUNNY table commands
        public const string BUNNY_INSERT = "INSERT INTO bunny(simulation_id, spawn_time, death_time, death_type) VALUES(?,?,?,?); SELECT last_insert_rowid()";
        public const string BUNNY_UPDATE = "UPDATE bunny SET death_time = ?, death_type = ? WHERE id = ?";
        public const string BUNNY_SELECT = "SELECT * FROM bunny WHERE id = ?";
        public const string BUNNY_SELECT_ALL = "SELECT * FROM bunny";
        public const string BUNNY_DELETE = "DELETE FROM bunny WHERE id = ?";
        
        //FOX table commands
        public const string FOX_INSERT = "INSERT INTO fox(simulation_id, spawn_time, death_time, death_type) VALUES(?,?,?,?); SELECT last_insert_rowid()";
        public const string FOX_UPDATE = "UPDATE fox SET death_time = ?, death_type = ? WHERE id = ?";
        public const string FOX_SELECT = "SELECT * FROM fox WHERE id = ?";
        public const string FOX_SELECT_ALL = "SELECT * FROM fox";
        public const string FOX_DELETE = "DELETE FROM fox WHERE id = ?";
    }
}