﻿//Author : Yannick Cote
namespace Play.Sql.Tables
{
    public class SimulationTable
    {
        public int Id
        {
            get;
            set;
        }

        public int Seed
        {
            get;
            set;
        }
    }
}