﻿//Author : Yannick Cote
namespace Play.Sql.Tables
{
    public class BunnyTable
    {
        public int Id
        {
            get;
            set;
        }

        public int SimulationId
        {
            get;
            set;
        }
        public int SpawnTime
        {
            get;
            set;
        }

        public int DeathTime
        {
            get;
            set;
        }
        
        public string DeathType
        {
            get;
            set;
        }
    }
}