﻿using System.Data.Common;
//Author : Yannick Cote
namespace Play.Sql
{
    public abstract class UtilRepository
    {
        protected DbConnection connection;
        private DbTransaction transaction;

        public UtilRepository(DbConnection connection, DbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }
        
        public DbCommand CreateParameter<T>(T value, DbCommand cmd)
        {
            var parameter = cmd.CreateParameter();
            parameter.Value = value;
            cmd.Parameters.Add(parameter);
            return cmd;
        }

        public void QueryCompletion()
        {
            transaction.Commit();
            connection.Close();
        }
    }
}