﻿using System;
using System.Data.Common;
using Play.Sql;
using Play.Sql.Tables;
using UnityEngine;
//Author : Jeammy Cote
namespace Game
{
    public sealed class Statistics : MonoBehaviour
    {
        private DbConnection connection;
        private DbTransaction transaction;
        private SimulationRepository simulationRepo;
        private BunnyRepository bunnyRepo;
        private FoxRepository foxRepo;
        private SimulationTable simulationTable;
        private void Start()
        {
            var (connection, transaction) = ConnectionToDatabase.StartConnectionToDatabase();
            this.connection = connection;
            this.transaction = transaction;
            
            try
            {
                bunnyRepo = new BunnyRepository(connection, transaction);
                foxRepo = new FoxRepository(connection,transaction);
                simulationRepo = new SimulationRepository(connection, transaction);
                
                WriteOnSimulationRepo();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                connection.Close();
                throw;
            }
        }

        public BunnyTable CreateBunnyOnRepo(BunnyTable bunnyTable)
        {
            try
            {
                bunnyTable.SimulationId = simulationTable.Id;
                var returnValue = bunnyRepo.Insert(bunnyTable);

                return returnValue;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                connection.Close();
                throw;
            }
        }
        public void WriteOnBunnyRepo(BunnyTable newTable, BunnyTable oldTable)
        {
            try
            {
                bunnyRepo.Update(newTable, oldTable);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                connection.Close();
                throw;
            }
        }

        public FoxTable CreateFoxOnRepo(FoxTable foxTable)
        {
            try
            {
                foxTable.SimulationId = simulationTable.Id;
                var returnValue = foxRepo.Insert(foxTable);

                return returnValue;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                connection.Close();
                throw;
            }
        }
        public void WriteOnFoxRepo(FoxTable newTable, FoxTable oldTable)
        {
            try
            {
                foxRepo.Update(newTable, oldTable);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                connection.Close();
                throw;
            }
        }
        private void WriteOnSimulationRepo()
        {
            try
            {
                simulationTable = new SimulationTable();
                simulationTable.Seed = Finder.RandomSeed.Seed;
                simulationTable.Id = simulationRepo.Insert(simulationTable).Id;
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                connection.Close();
                throw;
            }
        }

        private void OnDestroy()
        {
            foxRepo.QueryCompletion();
        }
    }
}