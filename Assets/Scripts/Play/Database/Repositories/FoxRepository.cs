﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Play.Sql.Tables;
//Author : Yannick Cote
namespace Play.Sql
{
    public class FoxRepository : UtilRepository, IRepository<FoxTable>
    {
        public FoxRepository(DbConnection connection, DbTransaction transaction) 
            : base(connection, transaction)
        {
        }
        public FoxTable Insert(FoxTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.FOX_INSERT;
            cmd = CreateParameter(item.SimulationId, cmd);
            cmd = CreateParameter(item.SpawnTime, cmd);
            cmd = CreateParameter(item.DeathTime, cmd);
            cmd = CreateParameter(item.DeathType, cmd);
            item.Id = Convert.ToInt32(cmd.ExecuteScalar());
            return item;
        }

        public FoxTable Update(FoxTable item, FoxTable itemToUpdate)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.FOX_UPDATE;
            cmd = CreateParameter(item.DeathTime, cmd);
            cmd = CreateParameter(item.DeathType, cmd);
            cmd = CreateParameter(itemToUpdate.Id, cmd);
            cmd.ExecuteNonQuery();
            return item;
        }

        public FoxTable Select(FoxTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.FOX_SELECT;
            cmd = CreateParameter(item.Id, cmd);
            cmd.ExecuteNonQuery();
            DbDataReader reader = cmd.ExecuteReader();
            FoxTable fox = new FoxTable();
            fox.Id = Convert.ToInt32(reader["id"]);
            fox.SimulationId = Convert.ToInt32(reader["simulation_id"]);
            fox.SpawnTime = Convert.ToInt32(reader["spawn_time"]);
            fox.DeathTime = Convert.ToInt32(reader["death_time"]);
            fox.DeathType = reader["death_type"].ToString();
            return fox;
        }

        List<FoxTable> IRepository<FoxTable>.SelectAll()
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.FOX_SELECT_ALL;
            cmd.ExecuteNonQuery();
            DbDataReader reader = cmd.ExecuteReader();
            
            List<FoxTable> listToReturn = new List<FoxTable>();
            while (reader.Read())
            {
                FoxTable foxToAdd = new FoxTable();
                foxToAdd.Id = Convert.ToInt32(reader["id"]);
                foxToAdd.DeathTime = Convert.ToInt32(reader["death_time"]);
                foxToAdd.DeathType = reader["death_type"].ToString();
                foxToAdd.SpawnTime = Convert.ToInt32(reader["spawn_time"]);
                foxToAdd.SimulationId = Convert.ToInt32(reader["simulation_id"]);
                listToReturn.Add(foxToAdd);
            }
            return listToReturn;
        }

        public void Delete(FoxTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.FOX_DELETE;
            cmd = CreateParameter(item.Id, cmd);
            cmd.ExecuteNonQuery();
        }
    }
}