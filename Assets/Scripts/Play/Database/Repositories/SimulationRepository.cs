﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Game;
using Harmony;
using Mono.Data.Sqlite;
using Play.Sql.Tables;
using SqliteDataReader = Mono.Data.SqliteClient.SqliteDataReader;
//Author : Yannick Cote
namespace Play.Sql
{
    public class SimulationRepository : UtilRepository, IRepository<SimulationTable>
    {
        private DbConnection connection;
        private DbTransaction transaction;
        public SimulationRepository(DbConnection connection, DbTransaction transaction) 
            : base(connection, transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public SimulationTable Insert(SimulationTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.SIM_INSERT;
            cmd = CreateParameter(item.Seed, cmd);
            item.Id = Convert.ToInt32(cmd.ExecuteScalar());
            return item;
        }

        public SimulationTable Update(SimulationTable item, SimulationTable itemToUpdate)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.SIM_UPDATE;
            cmd = CreateParameter(item.Seed, cmd);
            cmd = CreateParameter(itemToUpdate.Id, cmd);
            cmd.ExecuteNonQuery();
            return item;
        }

        public SimulationTable Select(SimulationTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.SIM_SELECT;
            cmd = CreateParameter(item.Id, cmd);
            cmd.ExecuteNonQuery();
            DbDataReader reader = cmd.ExecuteReader();
            SimulationTable simulationToAdd = new SimulationTable();
            simulationToAdd.Id = Convert.ToInt32(reader["id"]);
            simulationToAdd.Seed = Convert.ToInt32(reader["seed"]);
            return simulationToAdd;
        }

        List<SimulationTable> IRepository<SimulationTable>.SelectAll()
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.SIM_SELECT_ALL;
            cmd.ExecuteNonQuery();
            DbDataReader reader = cmd.ExecuteReader();
            
            List<SimulationTable> listToReturn = new List<SimulationTable>();
            while (reader.Read())
            {
                SimulationTable simulationToAdd = new SimulationTable();
                simulationToAdd.Id = Convert.ToInt32(reader["id"]);
                simulationToAdd.Seed = Convert.ToInt32(reader["seed"]);
                listToReturn.Add(simulationToAdd);
            }
            return listToReturn;
        }

        public void Delete(SimulationTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.SIM_DELETE;
            cmd = CreateParameter(item.Id, cmd);
            cmd.ExecuteNonQuery();
        }
    }
}