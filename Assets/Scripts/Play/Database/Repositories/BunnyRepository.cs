﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Play.Sql.Tables;
//Author : Yannick Cote
namespace Play.Sql
{
    public class BunnyRepository : UtilRepository, IRepository<BunnyTable>
    {
        public BunnyRepository(DbConnection connection, DbTransaction transaction)
            : base(connection, transaction)
        {
        }

        public BunnyTable Insert(BunnyTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.BUNNY_INSERT;
            cmd = CreateParameter(item.SimulationId, cmd);
            cmd = CreateParameter(item.SpawnTime, cmd);
            cmd = CreateParameter(item.DeathTime, cmd);
            cmd = CreateParameter(item.DeathType, cmd);
            item.Id = Convert.ToInt32(cmd.ExecuteScalar());
            return item;
        }

        public BunnyTable Update(BunnyTable item, BunnyTable itemToUpdate)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.BUNNY_UPDATE;
            cmd = CreateParameter(item.DeathTime, cmd);
            cmd = CreateParameter(item.DeathType, cmd);
            cmd = CreateParameter(itemToUpdate.Id, cmd);
            cmd.ExecuteNonQuery();
            return item;
        }

        public BunnyTable Select(BunnyTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.BUNNY_SELECT;
            cmd = CreateParameter(item.Id, cmd);
            cmd.ExecuteNonQuery();
            DbDataReader reader = cmd.ExecuteReader();
            BunnyTable bunny = new BunnyTable();
            bunny.Id = Convert.ToInt32(reader["id"]);
            bunny.SimulationId = Convert.ToInt32(reader["simulation_id"]);
            bunny.SpawnTime = Convert.ToInt32(reader["spawn_time"]);
            bunny.DeathTime = Convert.ToInt32(reader["death_time"]);
            bunny.DeathType = reader["death_type"].ToString();
            return bunny;
        }

        List<BunnyTable> IRepository<BunnyTable>.SelectAll()
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.BUNNY_SELECT_ALL;
            cmd.ExecuteNonQuery();
            DbDataReader reader = cmd.ExecuteReader();
            
            List<BunnyTable> listToReturn = new List<BunnyTable>();
            while (reader.Read())
            {
                BunnyTable bunnyToAdd = new BunnyTable();
                bunnyToAdd.Id = Convert.ToInt32(reader["id"]);
                bunnyToAdd.DeathTime = Convert.ToInt32(reader["death_time"]);
                bunnyToAdd.DeathType = reader["death_type"].ToString();
                bunnyToAdd.SpawnTime = Convert.ToInt32(reader["spawn_time"]);
                bunnyToAdd.SimulationId = Convert.ToInt32(reader["simulation_id"]);
                listToReturn.Add(bunnyToAdd);
            }
            return listToReturn;        }

        public void Delete(BunnyTable item)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = SqlCommands.BUNNY_DELETE;
            cmd = CreateParameter(item.Id, cmd);
            cmd.ExecuteNonQuery();        }
    }
}