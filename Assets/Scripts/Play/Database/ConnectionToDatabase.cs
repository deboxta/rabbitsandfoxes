﻿using System.Data.Common;
using Game;
//Author Yannick Cote
namespace Play.Sql
{
    public static class ConnectionToDatabase
    {
        public static (DbConnection, DbTransaction) StartConnectionToDatabase()
        {
            var connection = Finder.SqLiteConnectionFactory.GetConnection();
            
            connection.Open();
            var transaction = connection.BeginTransaction();
            return (connection, transaction);
        }
    }
}