﻿//Author : Jeammy Cote
namespace Game
{
    public static class TypeOfDeath
    {
        public const string HUNGER = "Hunger";
        public const string THIRST = "Thirst";
        public const string EATED = "Eated";
    }
}