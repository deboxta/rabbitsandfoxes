﻿namespace Game
{
    //Author : Jeammy Côté
    public static class StatesTag
    {
        public const string FOXWALKINGSTATE = "FoxWalkingState";
        public const string FOXHUNTINGSTATE = "FoxHuntingState";
        public const string FOXSEARCHFOODSTATE = "FoxSearchFoodState";
        public const string FOXHORNYSTATE = "FoxHornyState";
        public const string BUNNYSEARCHFOODSTATE = "BunnySearchFoodState";
        public const string BUNNYFLEESTATE = "BunnyFleeState";
        public const string BUNNYWALKINGSTATE = "BunnyWalkingState";
        public const string BUNNYHORNYSTATE = "BunnyHornyState";
        public const string BUNNYTARGETINGSTATE = "BunnyTargetingState";
        public const string SEARCHWATERSTATE = "SearchWaterState";
    }
}